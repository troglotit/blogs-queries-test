(ns flocktest.core
  (:require [ring.adapter.jetty :as jetty]
            [flocktest.handler :as handler]))


(defn -main []
  (jetty/run-jetty handler/app {:port 3000}))
