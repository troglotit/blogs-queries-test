(ns flocktest.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.params :refer [wrap-params]]
            [flocktest.search :as search]
            ))

(defroutes app-routes
  (GET "/search" [] search/search)
  (route/not-found "Not Found"))

(def app
    (wrap-params app-routes))
