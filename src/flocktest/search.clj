(ns flocktest.search
  (:require [cheshire.core :as cheshire]
            [clj-http.client :as client]
            [cemerick.url :as url]
            [clojure.core.async :as async]
            [clojure.string :as string]
            [feedparser-clj.core :as fp]
            [clojure.core.async :as async :refer [chan close! go >! <! <!! >!! go-loop put! thread alts! alts!! timeout pipeline pipeline-blocking pipeline-async]]))

(defn get-words-vector [request]
  (let [request-value (get (:query-params request) "query")]
    (if (vector? request-value)
      request-value
      [request-value])))

(def yandex-blogs
  "http://blogs.yandex.ru/search.rss")

(defn string->stream
  ([s] (string->stream s "UTF-8"))
  ([s encoding]
   (-> s
       (.getBytes encoding)
       (java.io.ByteArrayInputStream.))))

(defn take-rss-map [query]
  (->> query
       (client/get yandex-blogs)
       :body
       string->stream
       clojure.java.io/input-stream
       fp/parse-feed))

(defn get-second-level-domain [hostname]
  (string/join "."
    (take-last 2
      (string/split hostname #"\."))))

(defn query->result [query]
  (->> query
       (hash-map :text)
       (hash-map :query-params)
       take-rss-map
       :entries
       (take 10)
       (map :link)
       set))

(defn process [queries]
  (let [to-ch (chan)
        from-ch (chan)
        parallelism 3]
  (pipeline-blocking parallelism to-ch (map query->result) from-ch)
  (doseq [query queries]
    (>!! from-ch query))
  (close! from-ch)
  (vec (<!! (async/reduce clojure.set/union #{} to-ch)))))

;; trivial processing version
;; (defn process [queries]
;;   (vec
;;    (apply clojure.set/union
;;           (map query->result queries))))

(defn search [request]
  (cheshire/generate-string
   (->> request
        get-words-vector
        process
        (map url/url)
        (map :host)
        (map get-second-level-domain)
        frequencies
        )
   {:pretty true}))
