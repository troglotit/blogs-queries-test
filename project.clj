(defproject flocktest "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/core.async "0.3.442"]
                 [ring/ring-defaults "0.2.1"]
                 [ring/ring-core "1.5.0"]
                 [ring/ring-jetty-adapter "1.5.0"]
                 [adamwynne/feedparser-clj "0.5.2"]
                 [com.cemerick/url "0.1.1"]
                 [cheshire "5.7.0"]
                 [compojure "1.5.1"]
                 [clj-http "2.3.0"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler flocktest.handler/app
         :port 3000}
  :main flocktest.core
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
